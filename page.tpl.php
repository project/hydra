<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?> 
</head>

<body class="<?php print $body_classes; ?>">

  <div id="topnav" class="clearfix">
    <div id="topnav-inner">
      <?php if ($secondary_links): ?>
        <?php print theme('links', $secondary_links, $attributes = array('id' => 'navleft')); ?>
      <?php endif; ?>
    </div><!-- /#topnav-inner -->
  </div><!-- /#topnav -->

  <div id="header">
    <div id="header-inner" class="clearfix">

      <div id="headerleft">
        <?php if ($logo): ?>
          <div id="logo">
            <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo-image" /></a>
          </div><!-- /#logo -->
        <?php endif; ?>
        <?php if ($site_name or $site_slogan): ?>
          <div id="site-title">
            <?php if ($site_slogan): ?>
              <h2>
                <?php print $site_slogan; ?>
              </h2>
            <?php endif; ?>
              <h1>
                <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home">
                  <?php print $site_name; ?>
                </a>
              </h1>
          </div><!-- /#site-title -->
        <?php endif; ?>
      </div><!-- /#headerleft -->

      <?php if ($search_box): ?>
        <div id="headerright">
          <?php print $search_box; ?>
        </div><!-- /#headerright -->
      <?php endif; ?>
			
    </div><!-- /#header-inner -->
  </div><!-- /#header -->

  <?php if ($primary_links or $feed_icons): ?>
    <div id="mainnav">
      <div id="mainnav-inner" class="clearfix">

        <?php if ($primary_links): ?>
          <div id="navlist">
            <?php print menu_tree($menu_name = 'primary-links');  ?>
          </div><!-- /#navlist -->
        <?php endif; ?>

        <?php if ($feed_icons): ?>
          <div id="rss">
            <?php print $feed_icons ?>
          </div><!-- /#rss -->
        <?php endif; ?>

      </div>
    </div><!-- /#mainnav -->
  <?php endif; ?>

  <?php if ($topdock or $mission): ?>
    <div id="top-dock">
      <div id="top-dock-inner">

        <?php if ($mission): ?>
          <div id="mission"> 
            <?php print $mission; ?>
          </div><!-- /#mission -->
        <?php endif; ?>

				<?php print $topdock; ?>
				
      </div><!-- /#top-dock-inner -->
    </div><!-- /#top-dock -->
  <?php endif; ?>

  <div id="page" class="clearfix">
    <div id="page-inner" class="clearfix">

      <div class="main <?php print phptemplate_mainwidth_class($left, $right, $topside, $bottomside); ?>">

        <?php if ($messages or $help): ?>
          <?php print $messages; ?>
          <?php print $help; ?>
        <?php endif; ?>

        <?php if ($breadcrumb): ?>
          <?php print $breadcrumb; ?>
        <?php endif; ?>

        <?php if ($tabs): ?>
          <div class="tabs">
            <?php print $tabs; ?>
          </div><!-- /.tabs -->
        <?php endif; ?>

      <?php if ($contenttoptop or $contenttopleft or $contenttopright or $contenttopbottom): ?>

        <?php if ($contenttoptop): ?>
          <div id="content-top-top">
            <?php print $contenttoptop; ?>
          </div>
        <?php endif; ?>

				<?php if ($contenttopleft or $contenttopright): ?>
					<div id="content-top-wrapper" class="clearfix">

						<?php if ($contenttopleft): ?>
						  <div id="content-top-left">
						    <?php print $contenttopleft; ?>
						  </div><!-- /#content-top-left -->
						<?php endif; ?>

						<?php if ($contenttopright): ?>
						  <div id="content-top-right">
						    <?php print $contenttopright; ?>
						  </div><!-- /#content-top-right -->
						<?php endif; ?>

					</div>
				<?php endif; ?>

        <?php if ($contenttopbottom): ?>
          <div id="content-top-bottom">
            <?php print $contenttopbottom; ?>
          </div><!-- /#content-top-bottom -->
        <?php endif; ?>

      <?php endif; ?>
  
        <div class="main-inner">

          <?php if (!$node): ?>
            <h1 class="title">
              <?php print $title; ?>
            </h1>
          <?php endif; ?>

					  <?php print $content; ?>
 
        </div><!-- /.main-inner  -->

       <?php if ($contentbottomtop or $contentbottomleft or $contentbottomright or $contentbottombottom): ?>

         <?php if ($contentbottomtop): ?>
           <div class="content-bottom-top">
             <?php print $contentbottomtop; ?>
           </div><!-- /#content-bottom-top  -->
         <?php endif; ?>

         <?php if ($contentbottomleft or contentbottomright): ?>
           <div id="content-bottom-wrapper" class="clearfix">

             <?php if ($contentbottomleft): ?>
               <div id="content-bottom-left">
                 <?php print $contentbottomleft; ?>
               </div><!-- /#content-bottom-bottomleft  -->
             <?php endif; ?>

             <?php if ($contentbottomright): ?>
               <div id="content-bottom-right">
                 <?php print $contentbottomright; ?>
               </div><!-- /#content-bottom-bottomright  -->
             <?php endif; ?>

          </div><!-- /#content-bottom-wrapper  -->
         <?php endif; ?>

         <?php if ($contentbottombottom): ?>
           <div id="content-bottom-bottom">
             <?php print $contentbottombottom; ?>
           </div><!-- /#content-bottom-bottom -->
         <?php endif; ?>

       <?php endif; ?>

      </div><!-- /.main -->
			
      <?php if ($topside or $left or $right or $bottomside): ?>
        <div class="sidebar <?php print phptemplate_sidewidth_class($left, $right, $topside, $bottomside); ?>">

          <?php if ($topside): ?>
            <div id="topside">
              <?php print $topside; ?>
            </div><!-- /#topside -->
          <?php endif; ?>

          <?php if ($left or $right): ?>

            <?php if ($left): ?>
              <div id="leftbox">
                <?php print $left; ?>
              </div><!-- /#leftbox -->
            <?php endif; ?>

            <?php if ($right): ?>
              <div id="rightbox">
                <?php print $right; ?>
              </div><!-- /#rightbox -->
            <?php endif; ?>

					<?php endif; ?>

          <?php if ($bottomside): ?>
            <div id="bottomside">
              <?php print $bottomside; ?>
            </div><!-- /#bottomside -->
          <?php endif; ?>

        </div><!-- /.sidebar -->
      <?php endif; ?>

    </div><!-- /#page-inner -->
  </div><!-- /#page -->

  <?php if ($bottomdock): ?>
    <div id="bottom-dock">
      <div id="bottom-dock-inner">

				<?php print $bottomdock; ?>
				
      </div><!-- /#bottom-dock-inner -->
    </div><!-- /#bottom-dock -->
  <?php endif; ?>

  <?php if ($footerleftbox or $footermiddlebox or $footerrightbox): ?>
    <div id="footer-container">
      <div id="footer">
        <div id="footer-inner" class="clearfix">

          <?php if ($footerleftbox or $footermiddlebox): ?>
        			<div id="footer-wrapper">
      	        <?php if ($footerleftbox): ?>
    	            <div id="footer-left">
    	              <?php print $footerleftbox; ?>
    	            </div><!-- /#footer-left-->
    	          <?php endif; ?>

      	        <?php if ($footermiddlebox): ?>
      	          <div id="footer-middle">
      	            <?php print $footermiddlebox; ?>
      	          </div><!-- /#footer-middle -->
      	        <?php endif; ?>
	      		  </div><!-- /#footer-right -->
          <?php endif; ?>			

          <?php if ($footerrightbox): ?>
            <div id="footer-right">
              <?php print $footerrightbox; ?>
            </div><!-- /#footer-right -->
          <?php endif; ?>

        </div><!-- /#footer-inner -->
      </div><!-- /#footer -->
    </div><!-- /#footer-container -->
  <?php endif; ?>

  <?php if ($footer_message): ?>
    <div id="footer-message-container">
      <div id="footer-message">
        <div id="footer-message-inner">
          <?php print $footer_message; ?>
        </div><!-- /#footer-message-inner -->
      </div><!-- /#footer-message -->
    </div><!-- /#footer-message-container -->
  <?php endif; ?>

  <?php print $closure ?>
  <?php print $scripts ?>
	
</body>

</html>
